package com.whizzp.web.Model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	/**
	 * The name that is shown to everyone
	 */
	private Long facebookId;
	private String token;
	private String firstName;
	/**
	 * Surname
	 */
	private String lastName;

	/**
	 * A valid username email address.
	 */
	private String username;
	/**
	 * 
	 */
	private String profilePicture;
	@ManyToOne
	@JoinColumn(name = "user_university_id")
	private University university;
	/**
	 * Gender
	 */
	private Short gender;
	/**
	 * Ranking between 0 and 50000. It will be shown by ranking/10000 and the two
	 * last digit is not shown.
	 */
	private Short ranking = 0;
	/**
	 * The number of people who rank this user. It will be used when a new ranking
	 * is calculated.
	 */
	private Integer rankingCount = 0;

	/**
	 * 
	 */
	private String bio;

	private Integer likePercent = 50;

	public Integer getLikePercent() {
		return likePercent;
	}

	public void setLikePercent(Integer likePercent) {
		this.likePercent = likePercent;
	}

	public User(String username, Long facebookId) {
		this.username = username;
		this.facebookId = facebookId;
	}

	User() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(Long facebookId) {
		this.facebookId = facebookId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	/*
	 * public Token getToken() { return token; } public void setToken(Token token) {
	 * this.token = token; }
	 */
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public Short getGender() {
		return gender;
	}

	public void setGender(Short gender) {
		this.gender = gender;
	}

	public Short getRanking() {
		return ranking;
	}

	public void setRanking(Short ranking) {
		this.ranking = ranking;
	}

	public Integer getRankingCount() {
		return rankingCount;
	}

	public void setRankingCount(Integer rankingCount) {
		this.rankingCount = rankingCount;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

}
