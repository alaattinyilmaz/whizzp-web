package com.whizzp.web.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.whizzp.web.Model.University;

public interface UniversityRepository extends CrudRepository<University, Long> {
	University findByid(Integer id);

	List<University> findByNameLike(String name);

	/**
	 * Returns the universities those names contains the given paramater.
	 * 
	 * @param name
	 * @return
	 */
	List<University> findByNameContaining(String name);

	/**
	 * Returns the universities of population between the given min-max.
	 * 
	 * @param minPopulation
	 * @param maxPopulation
	 * @return
	 */
	List<University> findByPopulationBetween(Integer minPopulation, Integer maxPopulation);

}
