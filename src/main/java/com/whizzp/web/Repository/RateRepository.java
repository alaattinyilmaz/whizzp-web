package com.whizzp.web.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.whizzp.web.Model.Rate;
import com.whizzp.web.Model.User;

public interface RateRepository extends CrudRepository<Rate, Long> {
	List<Rate> findByTo(User to);
}
