package com.whizzp.web.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.whizzp.web.Model.University;
import com.whizzp.web.Model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	User findById(Integer id);

	Optional<User> findByUsername(String username);

	List<User> findByUniversity(University university);

	List<User> findByGender(Short gender);

	List<User> findByRankingGreaterThanEqual(Integer minRanking);

	@Query("SELECT u FROM User u WHERE NOT EXISTS(SELECT i FROM Interaction i WHERE i.from = :from AND i.to = u)")
	List<User> findSwiepeableUsers(@Param("from") User from);
	
	@Query("SELECT u FROM User u WHERE NOT EXISTS(SELECT i FROM Rate i WHERE i.from = :from AND i.to = u)")
	List<User> findRateableUsers(@Param("from") User from);
	
	


}
