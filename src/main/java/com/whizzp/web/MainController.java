package com.whizzp.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.whizzp.web.Constants.InteractionType;
import com.whizzp.web.Model.Image;
import com.whizzp.web.Model.Interaction;
import com.whizzp.web.Model.Preferences;
import com.whizzp.web.Model.Rate;
import com.whizzp.web.Model.User;
import com.whizzp.web.Repository.ImageRepository;
import com.whizzp.web.Repository.InteractionRepository;
import com.whizzp.web.Repository.PreferencesRepository;
import com.whizzp.web.Repository.RateRepository;
import com.whizzp.web.Repository.UniversityRepository;
import com.whizzp.web.Repository.UserRepository;

/**
 * Handles the requests.
 * 
 * @author Kerim Gokarslan
 * 
 *
 */
@Controller
@RequestMapping(path = "/api")
public class MainController {
	/* Repository declarations, all of them are autowired. */
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private InteractionRepository interactionRepository;
	@Autowired
	private UniversityRepository universityRepository;
	@Autowired
	private PreferencesRepository preferencesRepository;
	@Autowired
	private RateRepository rateRepository;
	@Autowired
	private ImageRepository imageRepository;

	// @Autowired
	// private TokenRepository tokenRepository;
	@PostMapping(path = "/is_authorized")
	public @ResponseBody Iterable<String> isAutorized(Principal principal) {
		TreeSet<String> dummy = new TreeSet<String>();
		dummy.add("dummy");
		return dummy;
	}

	@PostMapping(path = "/get_swipeable_users")
	public @ResponseBody Iterable<User> getSwipeableUsers(Principal principal) {
		User user = this.validateUser(principal);
		// return userRepository.findSwiepeableUsers(user);
		return userRepository.findAll();
	}

	@PostMapping(path = "/get_rateable_users")
	public @ResponseBody Iterable<User> getRateableUsers(Principal principal) {
		User user = this.validateUser(principal);
		// return userRepository.findRateableUsers(user);
		return userRepository.findAll();
	}

	@PostMapping(path = "/add_interaction")
	public @ResponseBody String addNewInteraction(Principal principal, @RequestParam Integer to_id,
			@RequestParam Short type) {

		/* First set the users */
		User from = this.validateUser(principal);
		User to = userRepository.findById(to_id);
		/* Check if there exists a previous symmetric interaction */
		if (type == InteractionType.LIKE) {
			Interaction previousInteraction = interactionRepository.findFirstByFromAndTo(to, from);
			if (previousInteraction != null && previousInteraction.getType() == InteractionType.LIKE) {// previously
																										// liked
				previousInteraction.setType(InteractionType.MATCH);
				interactionRepository.save(previousInteraction);
				type = InteractionType.MATCH;
				// NOTIFY THE USERS
			}
		}
		Interaction interaction = new Interaction();
		interaction.setFrom(from);
		interaction.setTo(to);
		interaction.setType(type);
		interaction = interactionRepository.save(interaction);
		// like count update
		if (type == InteractionType.LIKE || type == InteractionType.DISLIKE) {
			int likeCount = interactionRepository.findByToAndType(to, InteractionType.LIKE).size();
			int dislikeCount = interactionRepository.findByToAndType(to, InteractionType.DISLIKE).size();
			int total = likeCount + dislikeCount;
			to.setLikePercent(likeCount * 100 / total);
		}
		return "The interaction is saved with id " + interaction.getId() + " typ: " + interaction.getType();

	}

	@PostMapping(path = "/get_all_interactions_from_user")
	public @ResponseBody Iterable<Interaction> getAllInteractionsOfUser(Principal principal) {
		// This returns a JSON or XML with the users
		User from = this.validateUser(principal);
		return interactionRepository.findByfrom(from);
	}

	@PostMapping(path = "/get_all_archived_from_user")
	public @ResponseBody Iterable<Interaction> getAllArchivedFromUser(Principal principal) {
		// This returns a JSON or XML with the users
		User from = this.validateUser(principal);
		return interactionRepository.findByFromAndType(from, InteractionType.ARCHIVE);
	}

	@PostMapping(path = "/get_all_liked_from_user")
	public @ResponseBody Iterable<Interaction> getAllLikedFromUser(Principal principal) {
		// This returns a JSON or XML with the users
		User from = this.validateUser(principal);
		return interactionRepository.findByFromAndType(from, InteractionType.LIKE);
	}

	@PostMapping(path = "/get_all_interactions_to_user")
	public @ResponseBody Iterable<Interaction> getAllInteractionsToUser(Principal principal) {
		// This returns a JSON or XML with the users

		User to = this.validateUser(principal);
		return interactionRepository.findByto(to);
	}

	@PostMapping(path = "/get_all_matches_of_user")
	public @ResponseBody Iterable<Interaction> getAllMatchesOfUser(Principal principal) {
		// This returns a JSON or XML with the users
		User from = this.validateUser(principal);
		// since matches are symmetric it doesn't matter whether checking from
		// 'from' or 'to' fields.
		return interactionRepository.findByFromAndType(from, InteractionType.MATCH);
	}

	@PostMapping(path = "/get_all_likes_of_user")
	public @ResponseBody Iterable<Interaction> getAllLikesOfUser(Principal principal) {
		User from = this.validateUser(principal);
		return interactionRepository.findByFromAndType(from, InteractionType.LIKE);
	}

	@PostMapping(path = "/get_users_with_ranking")
	public @ResponseBody Iterable<User> getUsersWithRanking(Principal principal, @RequestParam int minRanking) {
		return userRepository.findByRankingGreaterThanEqual(minRanking);
	}

	@PostMapping(path = "/get_users_in_university")
	public @ResponseBody Iterable<User> getUsersInUniversity(@RequestParam int id) {
		User user = userRepository.findById(id);
		return userRepository.findByUniversity(user.getUniversity());
	}

	@PostMapping(path = "/rate_user")
	public @ResponseBody String rateUser(Principal principal, @RequestParam int id, @RequestParam Short rate) {

		User from = this.validateUser(principal);
		User to = userRepository.findById(id);
		Rate newRate = new Rate();
		newRate.setFrom(from);
		newRate.setTo(to);
		newRate.setRate(rate);
		rateRepository.save(newRate);
		// TODO: trigger calc new rate
		calculateNewRate(to);
		return "";
	}

	@PostMapping(path = "/get_images_of_user")
	public @ResponseBody Iterable<Image> getImagesOfUser(Principal principal) {
		User user = this.validateUser(principal);
		return imageRepository.findByUser(user);

	}

	@PostMapping(path = "/add_image_to_user")
	public @ResponseBody String addImageToUser(Principal principal, @RequestParam String imageURL) {
		User user = this.validateUser(principal);
		Image image = new Image();
		image.setImageURL(imageURL);
		image.setUser(user);
		imageRepository.save(image);
		return "";

	}

	/**
	 * Returns the user with given id. An empty list if given id does not exist.
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(path = "/get_user")
	public @ResponseBody User getUserWithID(@RequestParam int id) {
		// This returns a JSON or XML with the users
		// ArrayList<Integer> list = new ArrayList<Integer>();
		// list.add(id);
		return userRepository.findById(id);
	}

	@PostMapping(path = "/get_preferences_of_user")
	public @ResponseBody Preferences getPreferencesOfUser(Principal principal) {
		User user = this.validateUser(principal);
		return preferencesRepository.findByUser(user);

	}

	@PostMapping(path = "/save_preferences_of_user")
	public @ResponseBody String savePreferencesOfUSer(Principal principal, @RequestParam boolean discoverable,
			@RequestParam Short maximumAge, @RequestParam Short maximumDistance, @RequestParam Short minimumAge,
			@RequestParam Short minimumRanking, @RequestParam Short preferredGender,
			@RequestParam Boolean universityFilter) {
		User user = this.validateUser(principal);
		Preferences preferences = preferencesRepository.findByUser(user);
		preferences.setDiscoverable(discoverable);
		preferences.setMaximumAge(maximumAge);
		preferences.setMaximumDistance(maximumDistance);
		preferences.setMinimumAge(minimumAge);
		preferences.setMinimumRanking(minimumRanking);
		preferences.setPreferredGender(preferredGender);
		preferences.setUniversityFilter(universityFilter);
		preferencesRepository.save(preferences);
		return "OK";
	}

	@PostMapping(path = "/get_myself")
	public @ResponseBody User getMyself(Principal principal) {
		return this.validateUser(principal);

	}

	private User validateUser(Principal principal) {
		String username = principal.getName();
		return this.userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
	}

	private void calculateNewRate(User to) {
		List<Rate> rates = rateRepository.findByTo(to);
		long sum = 0;
		for (Rate rate : rates) {
			sum += rate.getRate();
		}
		sum = sum * 10000 / rates.size();
		to.setRanking((short) sum);
		userRepository.save(to);

	}

}
