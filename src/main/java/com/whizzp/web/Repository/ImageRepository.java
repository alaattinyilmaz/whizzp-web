package com.whizzp.web.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.whizzp.web.Model.Image;
import com.whizzp.web.Model.User;

public interface ImageRepository extends CrudRepository<Image, Long>{
	List<Image> findByUser(User user);
}
