package com.whizzp.web.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.whizzp.web.Model.Interaction;
import com.whizzp.web.Model.User;

public interface InteractionRepository extends CrudRepository<Interaction, Long> {
	Interaction findByid(Integer id);

	Interaction findFirstByFromAndTo(User from, User to);

	List<Interaction> findByfrom(User from);

	List<Interaction> findByto(User to);

	List<Interaction> findBytype(Short type);

	List<Interaction> findByFromAndType(User from, Short type);
	
	List<Interaction> findByToAndType(User to, Short type);

	List<Interaction> findByFromAndDateGreaterThanEqual(User from, Date date);

}
