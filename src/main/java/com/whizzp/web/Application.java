package com.whizzp.web;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

import com.whizzp.web.Constants.Gender;
import com.whizzp.web.Model.Image;
import com.whizzp.web.Model.Preferences;
import com.whizzp.web.Model.User;
import com.whizzp.web.Repository.ImageRepository;
import com.whizzp.web.Repository.PreferencesRepository;
import com.whizzp.web.Repository.UserRepository;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/* TEST */
	// CORS
	@Bean
	FilterRegistrationBean corsFilter(@Value("${tagit.origin:http://localhost:9000}") String origin) {
		return new FilterRegistrationBean(new Filter() {
			public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
					throws IOException, ServletException {
				HttpServletRequest request = (HttpServletRequest) req;
				HttpServletResponse response = (HttpServletResponse) res;
				String method = request.getMethod();
				// this origin value could just as easily have come from a
				// database
				response.setHeader("Access-Control-Allow-Origin", origin);
				response.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE");
				response.setHeader("Access-Control-Max-Age", Long.toString(60 * 60));
				response.setHeader("Access-Control-Allow-Credentials", "true");
				response.setHeader("Access-Control-Allow-Headers",
						"Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization");
				if ("OPTIONS".equals(method)) {
					response.setStatus(HttpStatus.OK.value());
				} else {
					chain.doFilter(req, res);
				}
			}

			public void init(FilterConfig filterConfig) {
			}

			public void destroy() {
			}
		});
	}

	@Bean
	CommandLineRunner init(UserRepository userRepository , PreferencesRepository preferencesRepository, ImageRepository imageRepository ) {
		return (evt) -> Arrays.asList("kerimgokarslan", "emiralaattinyilmaz", "didemtok", "zeynepgulsahaygun").forEach(a -> {
			User user = new User(a, 11111l);
			user.setToken("token");
			
			if (a.contains("emiralaattinyilmaz")) {
				user.setFirstName("Emir Alaattin");
				user.setLastName("Yilmaz");
				user.setProfilePicture("https://scontent.fada1-4.fna.fbcdn.net/v/t1.0-9/17796749_1386770284720547_6676867554869722638_n.jpg?oh=fe5aaf977f79cf1502283c3c20b68630&oe=59D51554");
				user.setGender(Gender.MALE);
			} else if(a.contains("kerimgokarslan")) {
				user.setFirstName("Kerim");
				user.setLastName("Gokarslan");
				user.setProfilePicture("https://scontent.fada1-4.fna.fbcdn.net/v/t1.0-9/14716250_10208805180980806_595274226711146805_n.jpg?oh=d58a744b3ecf518b6de09785d7f3348d&oe=59C50E70");
				user.setGender(Gender.MALE);
			} else if(a.contains("didemtok")){
				user.setFirstName("Didem");
				user.setLastName("Tok");
				user.setProfilePicture("https://scontent.fada1-4.fna.fbcdn.net/v/t1.0-9/17308921_10210572689486924_1201874165554297045_n.jpg?oh=aeb928e42fc35f18f71b3fcf3330739c&oe=59D6F2FC");
				user.setGender(Gender.FEMALE);
			}else if(a.contains("zeynepgulsahaygun")){
				user.setFirstName("Zeynep Gulsah");
				user.setLastName("Aygun");
				user.setProfilePicture("https://scontent.fada1-4.fna.fbcdn.net/v/t1.0-9/1375006_682130631798592_849801477_n.jpg?oh=e518f3e6803a53362de2eccce970cb8a&oe=5A097181");
				user.setGender(Gender.FEMALE);
			}
			
			
			
			user = userRepository.save(user);
			Preferences preferences = new Preferences();
			preferences.setUser(user);
			preferencesRepository.save(preferences);
			Image image = new Image();
			image.setUser(user);
			image.setImageURL("https://www.petfinder.com/wp-content/uploads/2012/11/91615172-find-a-lump-on-cats-skin-632x475.jpg");
			imageRepository.save(image);
			
			image = new Image();
			image.setUser(user);
			image.setImageURL("http://i.internethaber.com/images/gallery/29426/3.jpg");
			imageRepository.save(image);
		
			image = new Image();
			image.setUser(user);
			image.setImageURL("http://images.cdn.starpulse.com/news/bloggers/1279398/blog_images/frank-1-1.jpg");
			imageRepository.save(image);
			
			image = new Image();
			image.setUser(user);
			image.setImageURL("http://s1.picofile.com/file/6268052312/boosh_.jpg");
			imageRepository.save(image);
			
			image = new Image();
			image.setUser(user);
			image.setImageURL("https://fthmb.tqn.com/IjBmJfcKV3qQ5sB_q9Llx_ifeV0=/1280x853/filters:no_upscale():fill(transparent,1)/about/african-grey-parrot-121805814-resized-58a6f0f65f9b58a3c919b22b.jpg");
			imageRepository.save(image);
			// Token token = new Token(user, "token");
			// token = tokenRepository.save(token);
			// user.setToken(token);

		});
	}
}
