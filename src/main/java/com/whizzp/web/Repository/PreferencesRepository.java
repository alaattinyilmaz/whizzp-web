package com.whizzp.web.Repository;

import org.springframework.data.repository.CrudRepository;

import com.whizzp.web.Model.Preferences;
//import com.whizzp.web.Model.User;
import com.whizzp.web.Model.User;

public interface PreferencesRepository extends CrudRepository<Preferences, Long> {

	Preferences findByUser(User user);

}
