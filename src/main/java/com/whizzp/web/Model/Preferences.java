package com.whizzp.web.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.whizzp.web.Constants.Gender;
import com.whizzp.web.Constants.PreferenceChoice;

@Entity
public class Preferences {
	

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@OneToOne
	@JoinColumn(name = "preference_user_id")
	private User user;
	private Short maximumDistance = PreferenceChoice.DISTANCE_MAX;
	private Short preferredGender = Gender.MALEANDFEMALE;
	private Short minimumAge = PreferenceChoice.AGE_MIN;
	private Short maximumAge = PreferenceChoice.AGE_MAX;
	private Boolean discoverable = PreferenceChoice.CHECKED;
	private Boolean universityFilter = PreferenceChoice.UNCHECKED;
	//private Boolean rankingFilter = PreferenceChoice.UNCHECKED;
	private Short minimumRanking = PreferenceChoice.RANKING_MIN;
	private Short maximumRanking = PreferenceChoice.RANKING_MAX;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Short getMaximumDistance() {
		return maximumDistance;
	}

	public void setMaximumDistance(Short maximumDistance) {
		this.maximumDistance = maximumDistance;
	}

	public Short getPreferredGender() {
		return preferredGender;
	}

	public void setPreferredGender(Short preferredGender) {
		this.preferredGender = preferredGender;
	}

	public Short getMinimumAge() {
		return minimumAge;
	}

	public void setMinimumAge(Short minimumAge) {
		this.minimumAge = minimumAge;
	}

	public Short getMaximumAge() {
		return maximumAge;
	}

	public void setMaximumAge(Short maximumAge) {
		this.maximumAge = maximumAge;
	}

	public Boolean getDiscoverable() {
		return discoverable;
	}

	public void setDiscoverable(Boolean discoverable) {
		this.discoverable = discoverable;
	}

	public Boolean getUniversityFilter() {
		return universityFilter;
	}

	public void setUniversityFilter(Boolean universityFilter) {
		this.universityFilter = universityFilter;
	}

	/*public Boolean getRankingFilter() {
		return rankingFilter;
	}

	public void setRankingFilter(Boolean rankingFilter) {
		this.rankingFilter = rankingFilter;
	}*/

	public Short getMinimumRanking() {
		return minimumRanking;
	}

	public void setMinimumRanking(Short minimumRanking) {
		this.minimumRanking = minimumRanking;
	}
}
